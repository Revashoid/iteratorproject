import java.util.Arrays;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.stream.Collectors;

public class MatrixIterator<E>{

    /*TODO: sorry, nagorodil govna zdes, takaya huinya*/

    private final E[][] matrix;
    private E elem;
    private int row, col;

    public MatrixIterator(E[][] matrix) {
        this.matrix = matrix;
    }

    public boolean hasNext() {
        return row < matrix.length && col < matrix.length;
    }

    public E next() {
        if(!hasNext()){
            throw new NoSuchElementException();
        }

        elem = matrix[row][col];
        col++;
        while(row < matrix.length && col >= matrix[row].length){
            col = 0;
            row++;
        }
        return elem;
    }

    public void remove() {
        var el = Arrays.stream(matrix).flatMap(array -> Arrays.stream(array).map(elem -> true)).findFirst();
        //Arrays.stream(matrix).filter(n -> Arrays.stream(n).filter(m -> m.equals(elem)));
        //Arrays.stream(matrix).filter(m -> Arrays.stream(m).filter(n -> n.equals(elem)));
        //Arrays.stream(matrix).filter(n -> n.equals(elem)); //gavno, ischet po massivam, pohui

/*        int index1 = 0, index2 = 0;
        for (E[] mrow: matrix) {
            for (E item: mrow) {
                if(item.equals(elem)){
                    matrix[index1][index2] = null; //TODO: gavno, kak blyat vzyat index bez etogo gazna (nikak)
                }
                index2++;
            }
            index1++;
        }*/

/*        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix.length; j++) {
                if(matrix[i][j].equals(elem)){
                    matrix[i][j] = null;
                }
            }*/
        }
    }