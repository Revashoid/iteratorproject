import java.util.*;

public class MIterator<E> {
    private int currentRow = 0;
    private int currentColumn = 0;
    private final E[][] matrix;

    public MIterator(E[][] matrix) {
        this.matrix = matrix;
    }

    public boolean hasNext() {
        if (currentRow + 1 == matrix.length) {
            return currentColumn < matrix[currentRow].length;
        }
        return currentRow < matrix.length;
    }

    public E next() {
        if (currentColumn == matrix[currentRow].length) {
            currentColumn = 0;
            currentRow++;
        }
        return matrix[currentRow][currentColumn++];
    }

    public void remove(){
        //matrix[currentRow][currentColumn - 1] = null;
        int index = currentColumn - 1;
        List<E> tmpList = new ArrayList<>();
        tmpList.addAll(Arrays.asList(matrix[currentRow]));
        tmpList.remove(currentColumn - 1);
        E[] tmparray = (E[]) tmpList.stream().toArray();
        matrix[currentRow] = (E[]) tmparray;
    }

    private E remove(int index) {
        final Object[] es = matrix[currentRow];

        @SuppressWarnings("unchecked") E oldValue = (E) es[index];
        fastRemove(es, index);

        return oldValue;
    }

    private void fastRemove(Object[] es, int i) {
        final int newSize =  matrix[currentRow].length - 1;
        System.arraycopy(es, i + 1, es, i, newSize - i);
    }
}
