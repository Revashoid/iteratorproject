import java.util.List;

public class ArrayIterator<E> {
    private int currentRow = 0;
    private int currentColumn = 0;
    private final List<List<E>> matrix;

    public ArrayIterator(List<List<E>> matrix) {
        this.matrix = matrix;
    }

    public boolean hasNext() {
        if (currentRow + 1 == matrix.size()) {
            return currentColumn < matrix.get(currentRow).size();
        }
        return currentRow < matrix.size();
    }

    public E next() {
        if (currentColumn == matrix.get(currentRow).size()) {
            currentColumn = 0;
            currentRow++;
        }
        return matrix.get(currentRow).get(currentColumn++);
    }

    public void remove(){
        matrix.get(currentRow).remove(currentColumn - 1);
        currentColumn--;
    }
}
