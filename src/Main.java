import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        newmain();
        //lastmain();
        //test();
    }

    private static void newmain(){
        List<List<Integer>> list = new ArrayList<>();
        list = fillList(list);

        ArrayIterator<Integer> arrayIterator = new ArrayIterator<>(list);
        while (arrayIterator.hasNext()){
            if(arrayIterator.next().equals(1)){
                arrayIterator.remove();
            }
        }

        ArrayIterator<Integer> showIterator = new ArrayIterator<>(list);
        while(showIterator.hasNext()){
            System.out.print(showIterator.next() + "\t");
        }
    }

    private static List<List<Integer>> fillList(List<List<Integer>> list){
        List<Integer> list1 = new ArrayList<>();
        list1.add(1);
        list1.add(3);
        list1.add(4);

        List<Integer> list2 = new ArrayList<>();
        list2.add(1);
        list2.add(4);
        list2.add(2);

        List<Integer> list3 = new ArrayList<>();
        list3.add(10);
        list3.add(9);
        list3.add(7);

        list.add(list1);
        list.add(list2);
        list.add(list3);

        return list;
    }

    private static void lastmain(){
        Integer[][] array = {{1,3,4}, {1,4,2}, {10,9,7}, {1,8,3}, {1,3}};

        MIterator<Integer> mIterator = new MIterator<>(array);
        while (mIterator.hasNext()){
            if(mIterator.next().equals(2)){
                mIterator.remove();
            }
        }

        for (Integer[] row: array) {
            for (Integer item: row) {
                System.out.print(item + "\t");
            }
        }
    }

    private static void test(){
        Integer[] array = {1,2,3,4,7,5};
        int index = 0;
        array[index] = null;

        Integer[] newarray = new Integer[array.length - 1];
        System.arraycopy(array, 0, newarray, 0, index);
        System.arraycopy(array, index + 1, newarray, index, array.length - index - 1);

        System.out.println(Arrays.toString(array));
        System.out.println(Arrays.toString(newarray));
    }
}
